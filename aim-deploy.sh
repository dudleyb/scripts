#!/usr/bin/env bash
#            ██
#           ░░
#   ██████   ██ ██████████
#  ░░░░░░██ ░██░░██░░██░░██
#   ███████ ░██ ░██ ░██ ░██
#  ██░░░░██ ░██ ░██ ░██ ░██
# ░░████████░██ ███ ░██ ░██
#  ░░░░░░░░ ░░ ░░░  ░░  ░░
# automated infra management
#
#  author ▓▒ dudley burrows <dudleyb@protonmail.com>
#    code ▓▒ https://gitlab.com/dudleyb/scripts
#
# this script was designed for running
# on a fresh rhel installation, with a second disk.
# after configuring zram it will install k3s
# then deploy awx and gitea

# ▓▓▒░ functions
function _echo() { printf "\n╓───── %s \n╙────────────────────────────────────── ─ ─ \n" "$1"; }

[ "$(id -u)" -ne 0 ] && {
	_echo "got root?" >&2
	exit 1
}

# ▓▓▒░ lvs
file -sL /dev/mapper/vg_k3s-kubelet | grep XFS >/dev/null || {
	# TODO
	# dynamically gather second device id
	_echo "setting up vg_k3s-kubelet"
	pvcreate /dev/vdb
	vgcreate vg_k3s /dev/vdb
	lvcreate -l +50%FREE -n kubelet vg_k3s
	lvcreate -l +100%FREE -n rancher vg_k3s
	mkfs.xfs /dev/mapper/vg_k3s-kubelet
	mkfs.xfs /dev/mapper/vg_k3s-rancher
	mkdir -p /var/lib/kubelet
	mkdir -p /var/lib/rancher
	mount /dev/mapper/vg_k3s-kubelet /var/lib/kubelet
	mount /dev/mapper/vg_k3s-rancher /var/lib/rancher
	grep kubelet /proc/mounts >> /etc/fstab
	grep rancher /proc/mounts >> /etc/fstab
}

# ▓▓▒░ zram
grep zram0 /proc/swaps >/dev/null 2>&1 || {
	_echo "configuring zram swap"
	[ -f /etc/modules-load.d/zram.conf ] || \
		echo zram > /etc/modules-load.d/zram.conf
	[ -f /etc/modprobe.d/zram.conf ] || \
		echo "options zram num_devices=1" > /etc/modprobe.d/zram.conf
	[ -f /etc/udev/rules.d/99-zram.rules ] || \
		cat << EOF >> /etc/udev/rules.d/99-zram.rules
KERNEL=="zram0", ATTR{disksize}="2G", TAG+="systemd"
EOF
	[ -f /etc/systemd/system/zram.service ] || \
		cat << EOF >> /etc/systemd/system/zram.service
[Unit]
Description=Swap with zram
After=multi-user.target

[Service]
Type=oneshot
RemainAfterExit=true
ExecStartPre=/sbin/mkswap /dev/zram0
ExecStart=/sbin/swapon /dev/zram0 -p 32767
ExecStop=/sbin/swapoff /dev/zram0

[Install]
WantedBy=multi-user.target
EOF
}

systemctl is-enabled zram.service >/dev/null 2>&1 || {
	_echo "enabling zram.service"
	systemctl enable zram.service
}

# ▓▓▒░ crypto policies
update-crypto-policies --show | grep "FUTURE" >/dev/null && {
	_echo "weaken crypto policies"
	update-crypto-policies --set DEFAULT
}

while true ; do
	read -p "reboot [y/n] " yn
	case $yn in
		[Yy]* ) systemctl reboot ;;
		[Nn]* ) break ;;
		* ) echo "yes or no" ;;
	esac
done

# ▓▓▒░ rhel subscription
subscription-manager status | grep "Overall Status: Current" >/dev/null || {
	_echo "rhn register and pkg install"
	subscription-manager register
	subscription-manager refresh
}

dnf install -y git ansible-core make

# ▓▓▒░ k3s
command -v kubectl >/dev/null || {
	_echo "install k3s"
	curl -sfL https://get.k3s.io > /tmp/k3s.sh
	chmod +x /tmp/k3s.sh
	sh /tmp/k3s.sh
}

echo "${PATH//:/$'\n'}" | grep "/usr/local/bin" >/dev/null || {
	_echo "source PATH"
	grep "/usr/local/bin" /root/.bash_profile || sed -i 's%^\(PATH=.*\)$%\1:/usr/local/bin%' /root/.bash_profile
	source /root/.bash_profile
}

# ▓▓▒░ firewall
systemctl is-active firewalld >/dev/null && {
	_echo "stopping firewall"
	systemctl stop firewalld >/dev/null
}

systemctl is-enabled firewalld >/dev/null && {
	_echo "disabling firewall"
	systemctl disable firewalld >/dev/null
}

# ▓▓▒░ awx-operator
[ -d /opt/awx-operator ] || {
	_echo "clone awx-operator repo"
	git clone https://github.com/ansible/awx-operator.git /opt/awx-operator
}

pushd /opt/awx-operator
git branch | grep "* (HEAD detached at 2.15.0)" >/dev/null || {
	_echo "checkout tag"
	git checkout tags/2.15.0
}

# ▓▓▒░ apiservices
for s in $(kubectl get apiservice | awk '/False/ {print $1}') ; do
	_echo "removing broken apiservice ${s}"
	kubectl delete apiservice "${s}"
done

# ▓▓▒░ deploy awx-operator
kubectl get ns awx >/dev/null 2>&1 || {
	_echo "deploy awx-operator"
	make deploy
	sleep 5
}

# ▓▓▒░ configure awx
[ -f awx-demo.yml ] && {
	_echo "rename awx deployment"
	sed -i 's/\(^.*name:\ awx\)-demo/\1/' awx-demo.yml
} || {
	_echo "configure awx"
	cat << EOF >> awx-demo.yml
---
apiVersion: awx.ansible.com/v1beta1
kind: AWX
metadata:
  name: awx
spec:
  service_type: nodeport
EOF
}

# ▓▓▒░ kustomize
[ -f kustomization.yaml ] || {
	_echo "kustomize awx"
	cat << EOF >> kustomization.yaml
---
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
resources:
  # Find the latest tag here: https://github.com/ansible/awx-operator/releases
  - github.com/ansible/awx-operator/config/default?ref=2.15.0
  - awx-demo.yml

# Set the image tags to match the git version from above
images:
  - name: quay.io/ansible/awx-operator
    newTag: 2.15.0

# Specify a custom namespace in which to install AWX
namespace: awx
EOF
}

# ▓▓▒░ deploy
_echo "deploy"
kubectl apply -k .

_echo "wait for deployment to finish"
while true ; do
	kubectl logs deployments/awx-operator-controller-manager -c awx-manager -n awx | grep "Ansible-runner exited successfully" >/dev/null && break
done
_echo "deployment finished"

# ▓▓▒░ ingress
[ -f ingress.yaml ] || {
	_echo "create ingress"
	cat << EOF >> ingress.yaml
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: awx-service
  annotations:
    ingress.kubernetes.io/ssl-redirect: "false"
    kubernetes.io/ingress.class: "traefik"
spec:
  rules:
  - http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: awx-service
            port:
              number: 80
EOF
	kubectl create -f ./ingress.yaml -n awx
}

_echo "access point"
kubectl describe ingress -n awx

_echo "admin password"
kubectl get secret awx-admin-password -n awx -o jsonpath="{.data.password}" | base64 --decode ; echo

# ▓▓▒░ helm
command -v helm >/dev/null || {
	_echo "installing helm"
	wget -O /tmp/helm.tar.gz https://get.helm.sh/helm-v3.14.4-linux-amd64.tar.gz
	tar xf /tmp/helm.tar.gz -C /tmp/
	mv /tmp/linux-amd64/helm /usr/local/bin/helm
}

grep KUBECONFIG /root/.bash_profile || {
	_echo "set kubeconfig for helm"
	cat << EOF >> /root/.bash_profile
KUBECONFIG=/etc/rancher/k3s/k3s.yaml
export KUBECONFIG
EOF
	source /root/.bash_profile
}

# ▓▓▒░ gitea
kubectl get ns gitea >/dev/null 2>&1 || {
	_echo "create gitea namespace"
	kubectl create ns gitea
}

[ -d /opt/gitea ] || {
	_echo "mkdir gitea"
	mkdir /opt/gitea
}

pushd /opt/gitea
[ -f gitea-values.yaml ] || {
	_echo "customise gitea"
	cat << EOF >> gitea-values.yaml
service:
  http:
    type: NodePort
    port: 3000
    nodePort: 30080
  ssh:
    type: NodePort
    port: 22
    ClusterIP: none
    nodePort: 30022
EOF
}

helm repo list | grep gitea-charts >/dev/null 2>&1 || {
	_echo "add gitea chart"
	helm repo add gitea-charts https://dl.gitea.com/charts
}

_echo "deploy gitea"
helm install --values /opt/gitea/gitea-values.yaml gitea gitea-charts/gitea -n gitea
