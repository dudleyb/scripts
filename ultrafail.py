#!/usr/bin/env python
#
# author:       dudley burrows <dudleyb@protonmail.com>
# created:      2019 09 13
# description:  uses the UltraDNS api to pull information
#               or change a traffic controller pool ip
#               state between 'Force Fail' and 'Normal'.
#               check the readme for install and usage.
#

import ultra_rest_client
import sys
import argparse

import requests
import json

def api_call(dns_domain):
    # load password
    creds = open("creds.txt", "r")
    creds = creds.readline().rstrip('\n')
    global username
    global password
    username = ""
    password = creds
    # static vars
    use_http = 'False'
    url = 'restapi.ultradns.com'
    # build api call
    global c
    c = ultra_rest_client.RestApiClient(username, password, 'True' == use_http, url)
    account_details = c.get_account_details()
    account_name = account_details[u'accounts'][0][u'accountName']
    all_zones = c.get_zones_of_account(account_name, offset=0, reverse=True)

    # check the specified domain exists
    # the api uses the term 'zone' instead of domain
    zone_name = None

    for zone in all_zones[u'zones']:
        if dns_domain == zone[u'properties'][u'name'][:-1]: # the last char is a period, strip it for compare
            domain_index = all_zones[u'zones'].index(zone)
            # set the specifed domain as the zone name for all future calls
            zone_name = all_zones[u'zones'][domain_index][u'properties'][u'name']

    # if the specified domain does not exist we error
    if zone_name is None:
        print("specified domain does not exist")
        sys.exit(1)

    # build domain calls using the specified domain
    rrsets = c.get_rrsets(zone_name)
    # we are only concerned with A type traffic controllers
    global rrsets_type
    rrsets_type = c.get_rrsets_by_type(zone_name, "A")

    return zone_name

## get the list of ips in traffic controller pool
def pool_select(zone_name, tc_pool, tc_ip):
    #global ip
    global rrsets_owner
    # get rrsets by type and ownerName (pool)
    rrsets_owner = c.get_rrsets_by_type_owner(zone_name, "A", tc_pool)

    # list of ips in pool records
    ip_list = rrsets_owner[u'rrSets'][0][u'rdata']

    # not all ips listed are in the pool,
    # ignore any that don't have a 'profile'
    if "profile" in rrsets_owner[u'rrSets'][0]:
        if tc_ip is not None:
            # find the ip specified
            global match
            match = [s for s in ip_list if tc_ip in s]
            # index ip to use against rdata
            ip_index = ip_list.index(match[0])
            # get details of ip
            pool_ip_details(tc_pool, tc_ip, ip_index)
        else:
            for ip in ip_list:
                tc_ip = ip
                # index ips to use against rdata
                ip_index = ip_list.index(tc_ip)
                # get details of ips
                pool_ip_details(tc_pool, tc_ip, ip_index)

# get and show details of ip
def pool_ip_details(tc_pool, tc_ip, ip_index):
    pool_details = rrsets_owner[u'rrSets'][0][u'profile'][u'rdataInfo'][ip_index]
    print("pool : {} \nip : {} \nstate : {}\n".format(tc_pool, tc_ip, pool_details[u'state']))

# get auth token for curl command
def auth_token():
    api_url = "https://api.ultradns.com/authorization/token"
    payload = "grant_type=password&username=" + username + "&password=" + password
    res = requests.post(api_url, data=payload).json()
    auth_token = res[u'accessToken']
    return auth_token

# set ip status to force fail
def force_fail(zone_name, tc_pool, tc_ip ):
    ## todo
    ## - figure out why the edit_rrset function doesn't accept profile
    #profile = "{u'@context':u'http://schemas.ultradns.com/TCPool.jsonschema',u'rdataInfo': [{u'state': u'NORMAL'}]}}"
    #print 'edit_rrset %s ' % c.edit_rrset(zone_name, "A", tc_pool, 30, [tc_ip ], profile=json.dumps(profile))
    try:
        token = auth_token()
    except:
        print("unable to get token")
        sys.exit(1)

    api_url = 'https://api.ultradns.com/zones/' + zone_name+ '/rrsets/A/' + tc_pool
    headers = {'Content-type': 'application/json', 'Authorization': 'Bearer ' + token + ''}
    payload = '{"rdata": ["' + tc_ip  + '"], "profile": {"@context":"http://schemas.ultradns.com/TCPool.jsonschema","rdataInfo": [{"state": "INACTIVE"}]}}'
    res = requests.patch(api_url, headers=headers, data=payload)
    if res.status_code == 200:
        print("%s successfully set to force fail" % tc_ip )
    else:
        print("error: %s" % res.json())

# set ip status to normal
def normal(zone_name, tc_pool, tc_ip ):
    token = auth_token()
    api_url = 'https://api.ultradns.com/zones/' + zone_name+ '/rrsets/A/' + tc_pool
    headers = {'Content-type': 'application/json', 'Authorization': 'Bearer ' + token + ''}
    payload = '{"rdata": ["' + tc_ip  + '"], "profile": {"@context":"http://schemas.ultradns.com/TCPool.jsonschema","rdataInfo": [{"state": "NORMAL"}]}}'
    res = requests.patch(api_url, headers=headers, data=payload)
    if res.status_code == 200:
        print("%s successfully set to normal" % tc_ip )
    else:
        print("error: %s" % res.json())

def main():

    # set global vars for later
    tc_pool = None # traffic controller pool name
    tc_ip = None # ip from pool records

    # parse arguments
    parser = argparse.ArgumentParser(description = "uses the UltraDNS api to pull information or change a traffic controller pool ip state between 'Force Fail' and 'Normal'.")
    parser.add_argument("-d", "--domain", help = "domain to probe", required = False)
    parser.add_argument("-f", "--file", help = "list of ips to probe", required = False, default = None, type = argparse.FileType('r'))
    parser.add_argument("--failover", help = "force ip(s) into force fail state", required = False, default = None, action='store_true')
    parser.add_argument("-i", "--ip", help = "ip to probe", required = False, default = None)
    parser.add_argument("--normal", help = "force ip(s) into normal state", required = False, default = None, action='store_true')
    parser.add_argument("-p", "--pool", help = "traffic controller pool to probe", required = False, default = None)
    argument = parser.parse_args()

    # set vars and ensure pool is set when specifying ip
    dns_domain = argument.domain
    if argument.pool:
        tc_pool = argument.pool
    if argument.ip and (tc_pool is None):
        parser.error("-i requires -p")
    else:
        tc_ip = argument.ip


    # read contents of file if required
    if argument.file:
        for line in argument.file:
            listedline = line.strip().split(' ')
            dns_domain = listedline[0]
            tc_pool = listedline[1]
            tc_ip = listedline[2]
            zone_name = api_call(dns_domain)
            pool_select(zone_name, tc_pool, tc_ip)
            if argument.failover is True:
                force_fail(zone_name, tc_pool, tc_ip)
            if argument.normal is True:
                normal(zone_name, tc_pool, tc_ip)
    elif tc_pool is not None:
        zone_name = api_call(dns_domain)
        pool_select(zone_name, tc_pool, tc_ip)
        if argument.failover is True:
            force_fail(zone_name, tc_pool, tc_ip)
        if argument.normal is True:
            normal(zone_name, tc_pool, tc_ip)
    else:
        zone_name = api_call(dns_domain)
        for lb_pool in rrsets_type[u'rrSets']:
                pool = lb_pool[u'ownerName']
                tc_pool = pool
                pool_select(zone_name, tc_pool, tc_ip)
                if argument.failover is True:
                    force_fail(zone_name, tc_pool, tc_ip)
                if argument.normal is True:
                    normal(zone_name, tc_pool, tc_ip)

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('\nyou stopped it')
        sys.exit(0)
