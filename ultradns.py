#!/usr/bin/env python
#
# author:       dudley burrows <dudleyb@protonmail.com>
# created:      2019 09 06
# description:  uses the UltraDNS api to pull information

import ultra_rest_client
import sys
import argparse

def main():
    # load password
    creds = open("creds", "r")
    creds = creds.readline().rstrip('\n')
    # static vars
    username = ""
    password = creds
    use_http = 'False'
    domain = 'restapi.ultradns.com'

    # set global vars for later
    tc_pool = None # traffic controller pool name
    tc_ip = None # ip from pool records

    # parse arguments
    parser = argparse.ArgumentParser(description = "get-token")
    parser.add_argument("-d", "--domain", help = "domain to probe", required = True)
    parser.add_argument("-i", "--ip", help = "ip to probe", required = False, default = None)
    parser.add_argument("-p", "--pool", help = "traffic controller pool to probe", required = False, default = None)
    argument = parser.parse_args()

    # set vars and ensure pool is set when specifying ip
    dns_domain = argument.domain
    if argument.pool:
        tc_pool = argument.pool
    if argument.ip and (tc_pool is None):
        parser.error("-i requires -p")
    else:
        tc_ip = argument.ip

    # build api call
    c = ultra_rest_client.RestApiClient(username, password, 'True' == use_http, domain)
    account_details = c.get_account_details()
    account_name = account_details[u'accounts'][0][u'accountName']
    all_zones = c.get_zones_of_account(account_name, offset=0, reverse=True)

    # check the specified domain exists
    # the api uses the term 'zone' instead of domain
    zone_name = None

    for zone in all_zones[u'zones']:
        if dns_domain == zone[u'properties'][u'name'][:-1]: # the last char is a period, strip it for compare
            domain_index = all_zones[u'zones'].index(zone)
            # set the specifed domain as the zone name for all future calls
            zone_name = all_zones[u'zones'][domain_index][u'properties'][u'name']

    # if the specified domain does not exist we error
    if zone_name is None:
        print("specified domain does not exist")
        sys.exit(1)

    # build domain calls using the specified domain
    rrsets = c.get_rrsets(zone_name)
    # we are only concerned with A type traffic controllers
    rrsets_type = c.get_rrsets_by_type(zone_name, "A")

    ## get the list of ips in traffic controller pool
    def pool_select(pool):
        # get rrsets by type and ownerName (pool)
        global rrsets_owner
        rrsets_owner = c.get_rrsets_by_type_owner(zone_name, "A", pool)

        # list of ips in pool records
        ip_list = rrsets_owner[u'rrSets'][0][u'rdata']

        # not all ips listed are in the pool,
        # ignore any that don't have a 'profile'
        if "profile" in rrsets_owner[u'rrSets'][0]:
            if tc_ip is not None:
                # find the ip specified
                global match
                match = [s for s in ip_list if tc_ip in s]

                # index ip to use against rdata
                i = ip_list.index(match[0])

                # get details of ip
                pool_ip_details(i)
            else:
                global ip
                for ip in ip_list:
                    # index ips to use against rdata
                    i = ip_list.index(ip)

                    # get details of ips
                    pool_ip_details(i)

    # get details of ip
    def pool_ip_details(pool_ip):
        pool_details = rrsets_owner[u'rrSets'][0][u'profile'][u'rdataInfo'][pool_ip]
        print("pool : {} \nip : {} \nstate : {}\n".format(tc_pool, ip, pool_details[u'state']))

    if tc_pool is not None:
        pool_select(tc_pool)
    else:
        for lb_pool in rrsets_type[u'rrSets']:
                pool = lb_pool[u'ownerName']
                tc_pool = pool
                pool_select(pool)


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('\nyou stopped it')
        sys.exit(0)
